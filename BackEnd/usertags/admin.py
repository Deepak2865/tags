from django.contrib import admin
from .models import User,UserTag,TagMaster

# Register your models here.
admin.site.register(User)
admin.site.register(UserTag)
admin.site.register(TagMaster)

