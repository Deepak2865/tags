from django.apps import AppConfig


class UsertagsConfig(AppConfig):
    name = 'usertags'
