# Generated by Django 3.0.3 on 2020-02-21 09:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('usertags', '0002_auto_20200214_1139'),
    ]

    operations = [
        migrations.RenameField(
            model_name='usertag',
            old_name='tagsmaster',
            new_name='tags',
        ),
    ]
