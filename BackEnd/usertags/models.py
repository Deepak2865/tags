from django.db import models

# Create your models here.
class User(models.Model):
    user_name=models.CharField(max_length=20,null=True)

    def __str__(self):
        return self.user_name

class TagMaster(models.Model):
    tag_title=models.CharField(max_length=30,unique=True)

    def __str__(self):
        return self.tag_title

    def save(self, *args, **kwargs) :
        self.tag_title = self.tag_title.lower()
        super().save(*args, **kwargs)

class UserTag(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    tags=models.ForeignKey(TagMaster,on_delete=models.CASCADE)

    def __str__(self):
        return '{} {}'.format(self.user,self.tags)
        

