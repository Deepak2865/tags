from rest_framework import serializers
from . import models


class TagMasterSerializers(serializers.ModelSerializer):
    class Meta:
        model=models.TagMaster
        fields = '__all__'

class UsersSerializers(serializers.ModelSerializer):
    class Meta:
        model=models.User
        fields='__all__'


class UserSerializer(serializers.ModelSerializer) :
    user_name = serializers.CharField(source='user.user_name')
    class Meta :
        model = models.UserTag
        fields = ['id',
            'user_name'
        ]

class UserTagSerializers(serializers.ModelSerializer):
    users = UserSerializer(many=True)
    class Meta:
        model=models.TagMaster
        fields=['id',
            'tag_title',
            'users'
        ]
        depth=1

class UserTagCreateSerializers(serializers.ModelSerializer):
    class Meta:
        model=models.UserTag
        fields='__all__'
        
        