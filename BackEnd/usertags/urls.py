from django.urls import path
from .views import TagMasterList,TagMasterCreate,UserListCreate,UserTagCreate,UserTagListView,UserTagList,TagMasterDelete,TagByName,FetchByName

urlpatterns = [
    path('tags/',TagMasterList.as_view()),
    path('tags/create/',TagMasterCreate.as_view()),
    path('users/',UserListCreate.as_view()),
    path('usertags/create/',UserTagCreate.as_view()),
    path('usertags/',UserTagListView.as_view()),
    path('usertags/<str:tag_title>',UserTagList.as_view(),name="title"),
    path('tags/<int:id>',TagMasterDelete.as_view()),
]