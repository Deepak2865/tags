from django.shortcuts import render
from .models import User,UserTag,TagMaster
from . import serializers
from rest_framework.response import Response
from rest_framework.parsers import FormParser, MultiPartParser, JSONParser
from rest_framework.generics import CreateAPIView,ListAPIView,ListCreateAPIView,RetrieveAPIView
from rest_framework import generics
from rest_framework.views import APIView
from django.http import HttpResponse
from django.db.models import Prefetch
from django.db.models import Q


class TagMasterList(ListAPIView):
    queryset=TagMaster.objects.all()
    serializer_class=serializers.TagMasterSerializers
    
class TagMasterCreate(CreateAPIView):
    queryset=TagMaster.objects.all()
    serializer_class=serializers.TagMasterSerializers
    def post(self,request,*args,**kwargs):
        try:
            list_tags=request.data['tag_title'].split(',')
            for tag in list_tags:
                TagMaster.objects.get_or_create(tag_title=tag)
            # TagMaster.objects.create(tag_title=str(list_tags))
            return Response(request.data, status=200)
        except TagMaster.DoesNotExist as e:
            return Response({"error":"already exists"})


class TagMasterDelete(APIView):
    def get_object(self,id):
        try:
            return TagMaster.objects.get(id=id)
        except TagMaster.DoesNotExist as e:
            return Response({'error':'not found'},status=404)

    def get(self,request,id=None):
        instance=self.get_object(id)
        serializer=serializers.TagMasterSerializers(instance)
        return Response(serializer.data)

    def delete(self,request,id=None):
        instance=self.get_object(id)
        instance.delete()
        return HttpResponse(status=204)

    
class UserListCreate(ListCreateAPIView):
    queryset=User.objects.all()
    serializer_class=serializers.UsersSerializers

class UserTagList(RetrieveAPIView):
    # queryset=UserTag.objects.all()
    serializer_class=serializers.UserTagSerializers
    
    def get_queryset(self) :
        queryset = TagMaster.objects.all().prefetch_related(
            Prefetch('usertag_set', UserTag.objects.all(), to_attr='users')
        )
        return queryset

    lookup_field = 'tag_title'
    lookup_url_kwarg = 'tag_title'


class UserTagListView(ListAPIView):
    # queryset=UserTag.objects.all()
    serializer_class=serializers.UserTagSerializers
    
    def get_queryset(self) :
        queryset = TagMaster.objects.all().prefetch_related(
            Prefetch('usertag_set', UserTag.objects.all(), to_attr='users')
        )
        return queryset
    


# class UserTagCreate(CreateAPIView):
#     queryset=UserTag.objects.all()
#     serializer_class=serializers.UserTagSerializers
    # def get_serializer_class(self):
    #     switcher = {
    #         'GET' : serializers.UserTagSerializers,
    #         'POST' : serializers.UserTagSerializers,
    #     }
    #     return switcher.get(self.request.method)

class UserTagCreate(CreateAPIView):
    queryset=UserTag.objects.all()
    serializer_class=serializers.UserTagCreateSerializers

# class TagByName(ListAPIView):
#     queryset=TagMaster.objects.all()
#     serializer_class=serializers.TagMasterSerializers
#     def get(self,request,*args,**kwargs):
#         tags=kwargs['tag_title']
#         TagId=TagMaster.objects.get(tag_title=tags)
#         return Response(TagId.id, status=200)

class TagByName(RetrieveAPIView):
    queryset=UserTag.objects.all()
    serializer_class=serializers.UserTagSerializers

    lookup_field = 'user'
    lookup_url_kwarg = 'user'


class FetchByName(ListAPIView):
    serializer_class=serializers.UserTagSerializers
    def get_queryset(self,*args,**kwargs):
        queryset_list=UserTag.objects.all()
        query=self.request.GET.get('q')
        if query:
            queryset_list=queryset_list.filter(
                Q(tag_title__icontains=query)
            )
        return queryset_list




     




