import React, { Component } from 'react'
import "./Tags.css" 
class Tags extends Component {
    constructor(props) {
        super(props)
        this.state = {
             tags:[],
             suggestions:[],
             activeSuggestion: 0,
             filteredSuggestions:[],
             showSuggestions:false,
             userInput:""
        }
    }
    componentDidMount(){
        fetch("http://127.0.0.1:8000/api/v1/tags/")
        .then(res=>res.json())
        .then((result)=>{
            this.setState({
                tags:result,
                suggestions:result
            })
        })
    }
    
    removeTags=(i)=>{
        fetch("http://127.0.0.1:8000/api/v1/tags/"+i,{
            method:"DELETE",
            headers:{'Accept':'application/json',
            'Content-Type':'application/json'}
            }) 
            const nTag=this.state.tags.filter(tag=>tag.id!==i);
            this.setState({tags:nTag,
            suggestions:nTag})  
    }
    addTags=(e)=>{
        let data={
            id:Math.round(Math.random()*1000),
            tag_title:this.state.userInput.trim()
        }   
        fetch("http://127.0.0.1:8000/api/v1/tags/create/",{method:"POST",
        body:JSON.stringify(data),
        headers:{'Content-Type':'application/json',
        'Accept':'application/json'}})
        .then(res=>res.json())
        .then((data)=>{
           if(this.state.tags.find(tag=>tag.tag_title===data.tag_title)){
                this.setState({tags:this.state.tags})
           }
           else{
               this.state.tags.push(data)
           }
            this.setState({tags:this.state.tags})
        },
        (error)=>{
            this.setState({tags:error})}
        )  
    }
    handleChange=(e)=>{ 
        const {suggestions}=this.state;
        const userInput=e.target.value;
        const newSuggestion = suggestions.filter((suggestion)=>{
        return suggestion.tag_title.toLowerCase().indexOf(userInput.toLowerCase())!==-1});
        this.setState({
            [e.target.name]:e.target.value,
            activeSuggestion:0,
            filteredSuggestions:[...newSuggestion],
            showSuggestions:true,
            userInput:e.target.value.toLowerCase()
        })
    }
    handleClick=(e)=>{
        this.setState({
            activeSuggestion:0,
            filteredSuggestions:[],
            showSuggestions:false,
            userInput:e.currentTarget.innerText
        })
        this.refs.userInput.focus()
    }
    handleKeyPress=(e)=>{
        if ((["Enter", "Tab", ","].includes(e.key)) && e.target.value!==",")
        {
            e.preventDefault();
            const userInput=this.state.userInput.trim()
            this.setState({userInput:userInput})
            if(userInput){
                this.addTags();
                this.setState({userInput:""})
            }
        }
    }
    onKeyDown=(e)=>{
        const {activeSuggestion,filteredSuggestions}=this.state;
        if (e.keyCode===13){
            this.setState({
                activeSuggestion:0,
                showSuggestions:false,
                userInput:filteredSuggestions[activeSuggestion]
            })
        }
        else if (e.keyCode===38){
            if (activeSuggestion===0){
                return;
            }
            this.setState({
                activeSuggestion:activeSuggestion-1
            })
        }
        else if (e.keyCode===40){
            if (activeSuggestion-1===filteredSuggestions.length){
                return;
            }
            this.setState({activeSuggestion:activeSuggestion+1})
        }
    };

render(){
    const{
        handleChange,
        handleClick,
        onKeyDown,
        handleKeyPress,
        state:{
            showSuggestions,
            filteredSuggestions,
            userInput,
            tags
        }
    }=this;
    let suggestionsListComponent;
        if (showSuggestions && userInput!=="") {
            if (filteredSuggestions.length >0) {
                suggestionsListComponent = (
                <ul className="suggestions">
                    {filteredSuggestions.map((suggestion) => {
                        return (
                            <li  
                            key={suggestion.id} 
                            onClick={handleClick}
                            style={{color:"violet"}}
                            >
                            {suggestion.tag_title}
                            </li>
                            );
                        })}
                </ul>
                );
            }   
            else {
                suggestionsListComponent = (
                <div className="no-suggestions">
                    <em>No suggestions!</em>
                </div>
                );
            }
        }
        return (
            <div>
                <div>
                    <input type="text"
                    name="userInput"
                    ref="userInput"
                    autoFocus
                    value={userInput}
                    placeholder="Press Enter to add tags"
                    onChange={handleChange}
                    // onKeyPress={handleKeyPress}
                    onKeyDown={handleKeyPress}
                    onClick={handleClick}
                    />
                </div>
                {suggestionsListComponent}
                <div className="tags-input">
                <ul id="tags">
                    {tags.map((tag, i) => (
                        <li key={i} className="tag">
                            {tag.tag_title}
                            <span className="tag-close-icon"
                            onClick={() => this.removeTags(tag.id)}
                            >x</span>
                        </li>
                    ))}
                </ul>
                </div>
            </div>
        )
    }
}   
export default Tags;